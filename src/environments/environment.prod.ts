import { AuthConfig } from "../app/core/security/auth.service";
export const environment = {
  production: true,

  search_url: "https://api.spotify.com/v1/search",
  auth_config: {
    auth_url: "https://accounts.spotify.com/authorize",
    client_id: "ecf9cbb765e7414fa57931cf80bae26a",
    redirect_uri: "http://localhost:4200/",
    response_type: "token"
  } as AuthConfig
};
