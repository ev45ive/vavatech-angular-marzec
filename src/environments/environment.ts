import { AuthConfig } from "../app/core/security/auth.service";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  search_url: "https://api.spotify.com/v1/search",
  auth_config: {
    auth_url: "https://accounts.spotify.com/authorize",
    client_id: "ecf9cbb765e7414fa57931cf80bae26a",
    redirect_uri: "http://localhost:4200/",
    response_type: "token"
  } as AuthConfig
};

// placki@placki.com : placki

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
