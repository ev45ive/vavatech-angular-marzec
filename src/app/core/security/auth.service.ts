import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export abstract class AuthConfig {
  auth_url!: string;
  client_id!: string;
  redirect_uri!: string;
  /**
   * Select "token"!
   */
  response_type!: "token" | "code";
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token = "";

  constructor(private config: AuthConfig) {}

  authorize() {
    const { auth_url, redirect_uri, response_type, client_id } = this.config;

    const p = new HttpParams({
      fromObject: {
        redirect_uri /* :redirect_uri */,
        response_type,
        client_id
      }
    });

    const url = `${auth_url}?${p.toString()}`;
    sessionStorage.removeItem("token");
    // document.location.href = (url);
    document.location.replace(url);
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token") || "null");

    if (document.location.hash && !this.token) {
      const p = new HttpParams({
        fromString: document.location.hash
      });
      this.token = p.get("#access_token") || "";
      sessionStorage.setItem("token", JSON.stringify(this.token));
      document.location.hash = "";
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
