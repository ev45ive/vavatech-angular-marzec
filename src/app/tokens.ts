import { InjectionToken } from '@angular/core';

// tokens.ts
export const MUSIC_SEARCH_URL = new InjectionToken<string>(
  "Url for music search api"
);
