import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { MusicModule } from "./music/music.module";
import { SecurityModule } from "./core/security/security.module";
import { SharedModule } from './shared/shared.module';
import { environment } from '../environments/environment';
import { MUSIC_SEARCH_URL } from './tokens';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    PlaylistsModule,
    // MusicModule,
    HttpClientModule,
    SharedModule,
    SecurityModule.forRoot(environment.auth_config),
    AppRoutingModule,
  ],
  providers: [
    {
      provide: MUSIC_SEARCH_URL,
      useValue: environment.search_url
    }
  ],
  bootstrap: [AppComponent /* ,WidgetComponent */]
  // entryComponents: [AppComponent],
})
export class AppModule {
  // constructor(private app:ApplicationRef){}
  // ngDoBootstrap() {
  //   this.app.bootstrap(AppComponent)
  //   console.log("hello");
  // }
}
