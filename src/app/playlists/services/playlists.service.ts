import { Injectable } from "@angular/core";
import { Playlist } from "../../models/playlist";
import { Observable, of, Observer as Placki } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  playlists: Playlist[] = [
    {
      id: 123,
      name: "Angular Hits!",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular TOP 20!",
      favorite: false,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "The Best of Angular",
      favorite: true,
      color: "#00ff00"
    }
  ];

  getPlaylists(): Observable<Playlist[]> {
    return of(this.playlists);
  }

  getPlaylist(id: Playlist["id"]) {
    return of(this.playlists.find(playlist => playlist.id == id));
  }

  constructor() {}
}
