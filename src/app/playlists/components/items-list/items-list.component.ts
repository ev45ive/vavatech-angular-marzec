import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from "../../../models/playlist";
import { NgForOfContext, NgForOf } from "@angular/common";

NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"]
  // encapsulation: ViewEncapsulation.Emulated,
  // inputs: ["playlists:items"]
})
export class ItemsListComponent implements OnInit {

  @Input("items")
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist | undefined;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  constructor() {}

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  myTrackFn(index:number, item:Playlist){
    return item.id
  }

  ngOnInit() {}
}
