import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "../../../models/playlist";
import { NgForm } from "@angular/forms";

enum MODES {
  show = "show",
  edit = "edit"
}

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist!: Playlist

  MODES = MODES;
  mode: "show" | "edit" = "show";

  constructor() {}

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistSaved = new EventEmitter<Playlist>();

  save(formRef: NgForm) {
    const draft = formRef.value as Partial<Playlist>;

    const playlist = {
      ...this.playlist,
      ...draft
    } as Playlist;

    this.playlistSaved.emit(playlist);
    this.mode = 'show'
  }

  ngOnInit() {}
}

// type Partial<T> = {
//   [key in keyof T]?: T[key]
// }
