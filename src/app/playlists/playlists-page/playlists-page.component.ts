import { Component, OnInit } from "@angular/core";
import { Playlist } from "../../models/playlist";
import { PlaylistsService } from "../services/playlists.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap, filter } from "rxjs/operators";

@Component({
  selector: "app-playlists-page",
  templateUrl: "./playlists-page.component.html",
  styleUrls: ["./playlists-page.component.scss"]
})
export class PlaylistsPageComponent implements OnInit {
  selected?: Playlist;

  playlists: Playlist[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {}

  savePlaylist(draft: Playlist) {
    // Replacing list:
    this.playlists = this.playlists.map(playlist =>
      playlist.id == draft.id ? draft : { ...playlist }
    );
    this.selected = draft;
  }

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id]);
  }

  ngOnInit() {
    this.route.paramMap
      .pipe(
        map(paramMap => paramMap.get("id")),
        map(id => parseInt(id!, 10)),
        switchMap(id => this.service.getPlaylist(id))
      )
      .subscribe(playlist => {
        if (playlist) {
          this.selected = playlist;
        }
      });

    this.service
      .getPlaylists()
      .subscribe(playlists => (this.playlists = playlists));
  }
}
