import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlaylistsPageComponent } from "./playlists-page/playlists-page.component";
import { PlaylistDetailsComponent } from "./components/playlist-details/playlist-details.component";
import { ItemsListComponent } from "./components/items-list/items-list.component";
import { ListItemComponent } from "./components/list-item/list-item.component";

import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module'

@NgModule({
  declarations: [
    PlaylistsPageComponent,
    PlaylistDetailsComponent,
    ItemsListComponent,
    ListItemComponent,
  ],
  imports: [CommonModule, FormsModule, SharedModule],
  exports: [PlaylistsPageComponent]
})
export class PlaylistsModule {}
