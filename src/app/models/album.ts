import { Artist } from "./Artist";
import { AlbumImage } from "./AlbumImage";
import { Entity } from "./Entity";

export interface WithHref {
  href?: string;
}

export interface Album extends Entity, WithHref {
  images: AlbumImage[];
  artists?: Artist[];
}

export interface PagingObject<T> {
  items: T[];
  limit: number;
  offset: number;
  total: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
