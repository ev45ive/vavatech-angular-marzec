import { Entity } from "./Entity";

export interface Artist extends Entity {
  popularity: number;
}
