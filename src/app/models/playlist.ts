// http://json2ts.com/

export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  /**
   * HEX Color
   */
  color: string;
  track?: Track[]
  // tracks: Array<Track>;
}

export interface Track {
  name: string;
}
