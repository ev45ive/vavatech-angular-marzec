import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsPageComponent } from "./playlists/playlists-page/playlists-page.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "music",
    loadChildren:'./music/music.module#MusicModule'
  },
  {
    path: "playlists",
    children: [
      {
        path: "",
        component: PlaylistsPageComponent
      },
      {
        path: ":id",
        component: PlaylistsPageComponent
      }
    ]
  },
  {
    path: "**",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
