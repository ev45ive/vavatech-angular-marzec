import { Component } from "@angular/core";
import { MusicSearchService } from "../services/music-search.service";
import { share, map, filter, scan, combineLatest } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-page",
  templateUrl: "./music-search-page.component.html",
  styleUrls: ["./music-search-page.component.scss"],
  viewProviders: [
    // MusicSearchService
  ]
})
export class MusicSearchPageComponent {
  message = "";

  query$ = this.service.getQuery();

  albums$ = this.service.getAlbums().pipe(share());

  loader$ = this.service.isLoading

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {}

  ngOnInit() {
    this.route.queryParamMap
      .pipe(
        map(queryParamMap => queryParamMap.get("q")),
        filter(Boolean)
      )
      .subscribe(query => {
        this.service.search(query);
      });
  }

  search(query: string) {
    this.router.navigate(["/music"], {
      queryParams: { q: query }
    });
  }
}
