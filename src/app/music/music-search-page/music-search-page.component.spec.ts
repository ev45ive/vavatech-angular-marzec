import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicSearchPageComponent } from './music-search-page.component';

describe('MusicSearchPageComponent', () => {
  let component: MusicSearchPageComponent;
  let fixture: ComponentFixture<MusicSearchPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicSearchPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicSearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
