import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album, AlbumsResponse } from "../../models/album";
import { HttpClient } from "@angular/common/http";

import {
  map,
  concat,
  startWith,
  switchAll,
  switchMap,
  filter,
  distinctUntilChanged,
  tap,
  catchError,
  delay
} from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import { MUSIC_SEARCH_URL } from '../../tokens';

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  private albumsChange = new BehaviorSubject<Album[]>([]);
  private loadingChange = new BehaviorSubject<boolean>(false);
  private queryChange = new BehaviorSubject<string>("batman");

  public isLoading = this.loadingChange.asObservable();

  constructor(
    @Inject(MUSIC_SEARCH_URL)
    private api_url: string,
    private http: HttpClient
  ) {
    this.queryChange
      .pipe(
        distinctUntilChanged(),
        tap(() => this.loadingChange.next(true)),
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.api_url, { params }).pipe(
            delay(2000),
            tap(() => this.loadingChange.next(false)),
            map(res => res.albums.items),
            catchError(() => {
              return [];
            })
          )
        )
        // switchAll(),
      )
      .subscribe(albums => {
        this.albumsChange.next(albums);
      });
  }

  search(query: string) {
    this.queryChange.next(query);
  }

  getQuery() {
    return this.queryChange.asObservable();
  }

  getAlbums() {
    return this.albumsChange.asObservable();
  }
}
