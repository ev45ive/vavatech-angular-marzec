import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchPageComponent } from "./music-search-page/music-search-page.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { AlbumsGridComponent } from "./components/albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    MusicSearchPageComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    MusicRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
    // SharedModule
  ],
  exports: [MusicSearchPageComponent],
  providers: [
    // {
    //   provide:'MusicService',
    //   // Name factory function and export!
    //   useFactory: (url:string)=>{
    //     return new MusicSearchService(url)
    //   },
    //   deps:['MUSIC_SEARCH_URL']
    // },
    // {
    //   provide: AbstractSearchService,
    //   useClass: SpotifyMusicSearchService
    //   // deps:['MUSIC_SEARCH_URL']
    // },
    // MusicSearchService
  ]
})
export class MusicModule {}
