import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchPageComponent } from './music-search-page/music-search-page.component';

const routes: Routes = [
  {
    path: /* music/ */ "",
    component: MusicSearchPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule {}
