import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  AbstractControl,
  FormControl,
  FormArray,
  FormBuilder,
  ValidatorFn,
  Validator,
  Validators,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  asyncCensor = (badword: string): AsyncValidatorFn => {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      // retrun this.get(url,params).pipe(map( resp => error))

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          const handler = setTimeout(() => {
            const hasError = (control.value as string).includes(badword);
            observer.next(hasError ? { censor: { badword } } : null);
            observer.complete();
          }, 500);

          return () => {
            clearTimeout(handler);
          };
        }
      );
    };
  };

  //
  censor = (badword: string): ValidatorFn => (
    control: AbstractControl
  ): ValidationErrors | null => {
    //
    const hasError = (control.value as string).includes(badword);

    return hasError ? { censor: { badword } } : null;
  };

  queryForm = this.bob.group({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // this.censor("placki")
      ],
      [this.asyncCensor("batman")]
    )
  });

  @Input()
  set query(query:string){
    this.queryForm.get('query')!.setValue(query,{
      emitEvent: false
      // onlySelf: true
    }) 
  }

  @Output()
  queryChange = new EventEmitter<string>();

  constructor(private bob: FormBuilder) {
    console.log(this.queryForm);

    const queryField = this.queryForm.get("query")!;

    const validChanges = queryField.statusChanges.pipe(
      filter(status => status == "VALID")
    );

    const valueChanges = queryField.valueChanges;

    const queryChanges = validChanges.pipe(
      withLatestFrom(valueChanges, (status, value) => value ),
      debounceTime(400),
      filter(q => q.length >= 3),
      distinctUntilChanged()
    );

    queryChanges.subscribe(query => this.search(query));
  }

  search(query: string) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
