import {
  Directive,
  ElementRef,
  Attribute,
  Input,
  OnInit,
  DoCheck,
  OnChanges,
  SimpleChanges,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]",
  exportAs:'placki'
  // host:{
  //   '[style.color]':'color'
  // }
})
export class HighlightDirective implements OnInit, DoCheck, OnChanges {
  isActive = false;

  @Input("appHighlight")
  appHighlight = 'red'

  @HostBinding("style.color")
  get color() {
    return this.isActive ? this.appHighlight : "initial";
  }

  constructor(private elem: ElementRef<HTMLElement>) {
    console.log('constructor')
  }

  @HostListener("mouseenter")
  activate() {
    this.isActive = true;
  }

  @HostListener("mouseleave")
  deactivate() {
    this.isActive = false;
  }

  ngOnInit(): void {
    // console.log("ngOnInit");
  }

  ngDoCheck(): void {
    // console.log("ngDoCheck");
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log("ngOnChanges", changes);
  }
}
// console.log(HighlightDirective)
