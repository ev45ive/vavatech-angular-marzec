import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HighlightDirective } from "./highlight.directive";
import { CardComponent } from "./card/card.component";
import { MusicProviderDirective } from "./music-provider.directive";

@NgModule({
  declarations: [HighlightDirective, CardComponent, MusicProviderDirective],
  imports: [CommonModule],
  exports: [HighlightDirective, CardComponent, MusicProviderDirective]
})
export class SharedModule {}
